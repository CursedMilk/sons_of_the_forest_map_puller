#!/usr/bin/env python

import requests
import os


def download_image(url, path):
    print(f"Attempting {url}...", end="")
    response = requests.get(url, stream=True)
    print(f" {response.status_code}")

    if not response.ok:
        return

    img_data = requests.get(url).content
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(f"{path}", "wb") as handler:
        handler.write(img_data)


base_url = "https://tiles.mapgenie.io/games/sons-of-the-forest/world/default-v1/"


for i in {13}:
    for j in range(4076, 4085):
        for k in range(4060, 4090):
            download_image(
                base_url + str(i) + "/" + str(j) + "/" + str(k) + ".jpg",
                "output/" + str(i) + "/" + str(j) + "/" + str(k) + ".jpg",
            )
