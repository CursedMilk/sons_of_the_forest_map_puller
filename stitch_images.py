#!/usr/bin/env python

from PIL import Image
import glob


def merge_images_vertically(image1, image2):
    (width1, height1) = image1.size
    (width2, height2) = image2.size

    result_width = max(width1, width2)
    result_height = height1 + height2

    result = Image.new("RGB", (result_width, result_height))
    result.paste(im=image1, box=(0, 0))
    result.paste(im=image2, box=(0, height1))
    return result


def merge_list_images_vertically(images):
    current_image = Image.open(images[0])
    for next_file in images[1:]:
        current_image = merge_images_vertically(current_image, Image.open(next_file))
    return current_image


def merge_images_horizontally(image1, image2):
    (width1, height1) = image1.size
    (width2, height2) = image2.size

    result_width = width1 + width2
    result_height = max(height1, height2)

    result = Image.new("RGB", (result_width, result_height))
    result.paste(im=image1, box=(0, 0))
    result.paste(im=image2, box=(width1, 0))
    return result


def merge_list_images_horizontally(images):
    current_image = Image.open(images[0])
    for next_file in images[1:]:
        current_image = merge_images_horizontally(current_image, Image.open(next_file))
    return current_image


vertical_strips = [
    ["output1.jpg", "output/13/4076/*"],
    ["output2.jpg", "output/13/4077/*"],
    ["output3.jpg", "output/13/4078/*"],
    ["output4.jpg", "output/13/4079/*"],
    ["output5.jpg", "output/13/4080/*"],
    ["output6.jpg", "output/13/4081/*"],
    ["output7.jpg", "output/13/4082/*"],
    ["output8.jpg", "output/13/4083/*"],
    ["output9.jpg", "output/13/4084/*"],
]

for i in vertical_strips:
    dirpath = i[1]
    files = sorted(glob.glob(dirpath))
    stitched_img_data = merge_list_images_vertically(files)
    stitched_img_data.save(i[0], quality=100)

final_stitched_img_data = merge_list_images_horizontally(
    [x[0] for x in vertical_strips]
)
final_stitched_img_data.save("final.jpg", quality=100)
